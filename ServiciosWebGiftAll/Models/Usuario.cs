﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosWebGiftAll.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }

        public List<Publicacion> Publicaciones { get; set; }

    }
}