﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace ServiciosWebGiftAll.Models
{
    public class Foto
    {
        public int Id { get; set; }
        public Byte[] Imagen { get; set; }
        public DateTime FechaCreacion { get; set; }

        public List<PublicacionFoto> Fotos { get; set; }

        public Foto()
        {

        }

    }
}