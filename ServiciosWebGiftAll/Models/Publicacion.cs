﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosWebGiftAll.Models
{
    public class Publicacion
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Ubicacion { get; set; }
        public DateTime FechaPublicacion { get; set; }
        public Usuario Usuario { get; set; }
        public int UsuarioId { get; set; }
        public Categoria Categoria { get; set; }
        public int CategoriaId { get; set; }

        public List<PublicacionFoto> Fotos { get; set; }

        public Publicacion()
        {

        }
    }
}