﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosWebGiftAll.Models
{
    public class PublicacionFoto
    {
        public int Id { get; set; }
        public Foto Fotos { get; set; }
        public int FotoId { get; set; }
        public Publicacion Publicaciones { get; set; }
        public int PublicacionId { get; set; }

    }
}