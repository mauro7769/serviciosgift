namespace ServiciosWebGiftAll.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class primeraMigracion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categorias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Fotoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Imagen = c.Binary(),
                        FechaCreacion = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PublicacionFotoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FotoId = c.Int(nullable: false),
                        PublicacionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fotoes", t => t.FotoId, cascadeDelete: true)
                .ForeignKey("dbo.Publicacions", t => t.PublicacionId, cascadeDelete: true)
                .Index(t => t.FotoId)
                .Index(t => t.PublicacionId);
            
            CreateTable(
                "dbo.Publicacions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Titulo = c.String(),
                        Descripcion = c.String(),
                        Ubicacion = c.String(),
                        FechaPublicacion = c.DateTime(nullable: false),
                        UsuarioId = c.Int(nullable: false),
                        CategoriaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categorias", t => t.CategoriaId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.UsuarioId)
                .Index(t => t.CategoriaId);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Correo = c.String(),
                        Password = c.String(),
                        Nombre = c.String(),
                        Apellido = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Publicacions", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.PublicacionFotoes", "PublicacionId", "dbo.Publicacions");
            DropForeignKey("dbo.Publicacions", "CategoriaId", "dbo.Categorias");
            DropForeignKey("dbo.PublicacionFotoes", "FotoId", "dbo.Fotoes");
            DropIndex("dbo.Publicacions", new[] { "CategoriaId" });
            DropIndex("dbo.Publicacions", new[] { "UsuarioId" });
            DropIndex("dbo.PublicacionFotoes", new[] { "PublicacionId" });
            DropIndex("dbo.PublicacionFotoes", new[] { "FotoId" });
            DropTable("dbo.Usuarios");
            DropTable("dbo.Publicacions");
            DropTable("dbo.PublicacionFotoes");
            DropTable("dbo.Fotoes");
            DropTable("dbo.Categorias");
        }
    }
}
