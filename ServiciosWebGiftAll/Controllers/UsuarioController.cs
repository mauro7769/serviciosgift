﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ServiciosWebGiftAll.Models;

namespace ServiciosWebGiftAll.Controllers
{
    public class UsuarioController : ApiController
    {
        private GiftDBContext context;

        public UsuarioController()
        {
            this.context = new GiftDBContext();
        }

        public IEnumerable<Object> get()
        {
            return context.Usuarios.ToList();
        }
        
        public int validar(string correo, string contrasena)
        {
            int id = 0;

            foreach (Usuario item in context.Usuarios)
            {
                if (item.Correo.Equals(correo) && item.Password.Equals(contrasena))
                {
                    id = item.Id;
                }
            }
            return id;
        }


        //api/Usuario/{correo,contrasena}
        public IHttpActionResult get(string correo, string contrasena)
        {
            int id = validar(correo, contrasena);

            Usuario usuario = context.Usuarios.Find(id);

            if (usuario == null)//404 notfound
            {
                return NotFound();
            }


            return Ok(usuario);//retornamos codigo 200 junto con el usuario buscado
        }

        //giftAllApi/Usuario
        public IHttpActionResult post(Usuario usuario)
        {

            context.Usuarios.Add(usuario);
            int filasAfectadas = context.SaveChanges();

            if (filasAfectadas == 0)
            {
                return InternalServerError();//500
            }

            return Ok(new { mensaje = "Agregado correctamente" });

        }


        //giftAllApi/Usuario/{id}
        public IHttpActionResult delete(int id)
        {
            //buscamos el usuario a eliminar
            Usuario usuario = context.Usuarios.Find(id);

            if (usuario == null) return NotFound();//404

            context.Usuarios.Remove(usuario);

            if (context.SaveChanges() > 0)
            {
                //retornamos codigo 200
                return Ok(new { Mensaje = "Eliminado correctamente" });
            }

            return InternalServerError();//500

        }


        //giftAllApi/Usuario/{id}
        public IHttpActionResult put(Usuario usuario)
        {
            context.Entry(usuario).State = System.Data.Entity.EntityState.Modified;

            if (context.SaveChanges() > 0)
            {
                return Ok(new { Mensaje = "Modificado correctamente" });
            }
            return InternalServerError();
        }
    }
}
