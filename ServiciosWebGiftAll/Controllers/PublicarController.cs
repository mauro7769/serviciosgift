﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ServiciosWebGiftAll.Models;

namespace ServiciosWebGiftAll.Controllers
{
    public class PublicarController : ApiController
    {
        private GiftDBContext context;

        public PublicarController()
        {
            this.context = new GiftDBContext();
        }

        public IEnumerable<Object> get()
        {
            return context.Publicaciones.Include("Usuarios").Include("Categorias").
                Select(p => new
                {

                    Id = p.Id,
                    Titulo = p.Titulo,
                    Descripcion = p.Descripcion,
                    Comuna = p.Ubicacion,
                    FechaPubliccacion = p.FechaPublicacion,
                Usuario = new
                {
                    Id = p.Usuario.Id,
                    Correo = p.Usuario.Correo,
                    Nombre = p.Usuario.Nombre,
                    Apellido = p.Usuario.Apellido

                },
                Categoria = new
                {
                    Id = p.Categoria.Id,
                    Nombre = p.Categoria.Nombre
                } 
            });
        }        


        //api/Publicar/{id}
        public IHttpActionResult get(int id)
        {
            Publicacion publicacion = context.Publicaciones.Find(id);

            if (publicacion == null)//404 notfound
            {
                return NotFound();
            }


            return Ok(publicacion);//retornamos codigo 200 junto con la publicacion buscada
        }

        //api/Usuario
        public IHttpActionResult post(Publicacion publicacion)
        {

            context.Publicaciones.Add(publicacion);
            int filasAfectadas = context.SaveChanges();

            if (filasAfectadas == 0)
            {
                return InternalServerError();//500
            }

            return Ok(new { mensaje = "Agregado correctamente" });

        }


        //api/Usuario/{id}
        public IHttpActionResult delete(int id)
        {
            //buscamos el usuario a eliminar
            Publicacion publicacion = context.Publicaciones.Find(id);

            if (publicacion == null) return NotFound();//404

            context.Publicaciones.Remove(publicacion);

            if (context.SaveChanges() > 0)
            {
                //retornamos codigo 200
                return Ok(new { Mensaje = "Eliminado correctamente" });
            }

            return InternalServerError();//500

        }



        public IHttpActionResult put(Publicacion publicacion)
        {
            context.Entry(publicacion).State = System.Data.Entity.EntityState.Modified;

            if (context.SaveChanges() > 0)
            {
                return Ok(new { Mensaje = "Modificado correctamente" });
            }
            return InternalServerError();
        }
    }
}
