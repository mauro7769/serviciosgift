﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ServiciosWebGiftAll.Models;

namespace ServiciosWebGiftAll.Controllers
{
    public class PublicacionFotoController : ApiController
    {
        private GiftDBContext context;

        public PublicacionFotoController()
        {
            this.context = new GiftDBContext();
        }

        public IEnumerable<Object> get()
        {
            return context.PublicacionFotos.Include("Publicacion").Include("Fotos").
                Select(p => new
                {

                    Id = p.Id,
                    Publicacion = new
                    {
                        Id = p.Publicaciones.Id,
                        Titulo = p.Publicaciones.Titulo,
                        Descripcion = p.Publicaciones.Descripcion,
                        Comuna = p.Publicaciones.Ubicacion,
                        FechaPubliccacion = p.Publicaciones.FechaPublicacion
                    },
                    Foto = new
                    {
                        Id = p.Fotos.Id,
                        Imagen = p.Fotos.Imagen,
                        FechaCreacion = p.Fotos.FechaCreacion

                    }
                });
        }


        //api/Publicar/{id}
        public IHttpActionResult get(int id)
        {
            PublicacionFoto publicacion = context.PublicacionFotos.Find(id);

            if (publicacion == null)//404 notfound
            {
                return NotFound();
            }


            return Ok(publicacion);//retornamos codigo 200 junto con la publicacion buscada
        }

        //api/Usuario
        public IHttpActionResult post(PublicacionFoto publicacion)
        {

            context.PublicacionFotos.Add(publicacion);
            int filasAfectadas = context.SaveChanges();

            if (filasAfectadas == 0)
            {
                return InternalServerError();//500
            }

            return Ok(new { mensaje = "Agregado correctamente" });

        }


        //api/Usuario/{id}
        public IHttpActionResult delete(int id)
        {
            //buscamos el usuario a eliminar
            PublicacionFoto publicacion = context.PublicacionFotos.Find(id);

            if (publicacion == null) return NotFound();//404

            context.PublicacionFotos.Remove(publicacion);

            if (context.SaveChanges() > 0)
            {
                //retornamos codigo 200
                return Ok(new { Mensaje = "Eliminado correctamente" });
            }

            return InternalServerError();//500

        }



        public IHttpActionResult put(PublicacionFoto publicacion)
        {
            context.Entry(publicacion).State = System.Data.Entity.EntityState.Modified;

            if (context.SaveChanges() > 0)
            {
                return Ok(new { Mensaje = "Modificado correctamente" });
            }
            return InternalServerError();
        }
    }
}
