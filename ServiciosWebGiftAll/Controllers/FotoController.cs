﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ServiciosWebGiftAll.Models;

namespace ServiciosWebGiftAll.Controllers
{
    public class FotoController : ApiController
    {
        private GiftDBContext context;

        public FotoController()
        {
            this.context = new GiftDBContext();
        }

        public IEnumerable<Object> get()
        {
            return context.Fotos.ToList();
        }


        //api/Categoria/{id}
        public IHttpActionResult get(int id)
        {
            Foto foto = context.Fotos.Find(id);

            if (foto == null)//404 notfound
            {
                return NotFound();
            }


            return Ok(foto);//retornamos codigo 200 junto con el usuario buscado
        }

        //api/Categoria
        public IHttpActionResult post(Foto foto)
        {

            context.Fotos.Add(foto);
            int filasAfectadas = context.SaveChanges();

            if (filasAfectadas == 0)
            {
                return InternalServerError();//500
            }

            return Ok(new { mensaje = "Agregado correctamente" });

        }


        //api/Foto/{id}
        public IHttpActionResult delete(int id)
        {
            //buscamos  a eliminar
            Foto foto = context.Fotos.Find(id);

            if (foto == null) return NotFound();//404

            context.Fotos.Remove(foto);

            if (context.SaveChanges() > 0)
            {
                //retornamos codigo 200
                return Ok(new { Mensaje = "Eliminado correctamente" });
            }

            return InternalServerError();//500

        }



        public IHttpActionResult put(Foto foto)
        {
            context.Entry(foto).State = System.Data.Entity.EntityState.Modified;

            if (context.SaveChanges() > 0)
            {
                return Ok(new { Mensaje = "Modificado correctamente" });
            }
            return InternalServerError();
        }
    }
}
