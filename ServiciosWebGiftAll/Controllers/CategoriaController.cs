﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ServiciosWebGiftAll.Models;

namespace ServiciosWebGiftAll.Controllers
{
    public class CategoriaController : ApiController
    {
        private GiftDBContext context;

        public CategoriaController()
        {
            this.context = new GiftDBContext();
        }

        public IEnumerable<Object> get()
        {
            return context.Categorias.ToList();
        }

        
        //api/Categoria/{id}
        public IHttpActionResult get(int id)
        {
            Categoria categoria = context.Categorias.Find(id);

            if (categoria == null)//404 notfound
            {
                return NotFound();
            }


            return Ok(categoria);//retornamos codigo 200 junto con el usuario buscado
        }             

        //api/Categoria
        public IHttpActionResult post(Categoria categoria)
        {

            context.Categorias.Add(categoria);
            int filasAfectadas = context.SaveChanges();

            if (filasAfectadas == 0)
            {
                return InternalServerError();//500
            }

            return Ok(new { mensaje = "Agregado correctamente" });

        }


        //api/Usuario/{id}
        public IHttpActionResult delete(int id)
        {
            //buscamos  a eliminar
            Categoria categoria = context.Categorias.Find(id);

            if (categoria == null) return NotFound();//404

            context.Categorias.Remove(categoria);

            if (context.SaveChanges() > 0)
            {
                //retornamos codigo 200
                return Ok(new { Mensaje = "Eliminado correctamente" });
            }

            return InternalServerError();//500

        }



        public IHttpActionResult put(Categoria categoria)
        {
            context.Entry(categoria).State = System.Data.Entity.EntityState.Modified;

            if (context.SaveChanges() > 0)
            {
                return Ok(new { Mensaje = "Modificado correctamente" });
            }
            return InternalServerError();
        }
    }
}
